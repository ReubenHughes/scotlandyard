package scotlandyard;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**
 * A class that contains all the information about a particular player.
 */

public class PlayerData implements User {

    private Player player;
    private Colour colour;
    private Integer location;
    public Map<Ticket, Integer> tickets;

    /**
     * Constructs a new PlayerData object.
     *
     * @param player the Player object associated with the player.
     * @param colour the colour of the player.
     * @param location the location of the player.
     * @param tickets the tickets associated with the player.
     */
    public PlayerData(Player player, Colour colour, Integer location, Map<Ticket, Integer> tickets) {
        this.player = player;
        this.colour = colour;
        this.location = location;
        this.tickets = tickets;
    }

    /**
     * Returns the Player object associated with the player.
     *
     * @return the Player object associated with the player.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Returns the Colour object associated with the player.
     *
     * @return the Colour object associated with the player.
     */
    public Colour getColour() {
        return colour;
    }

    /**
     * Sets the player's current location.
     *
     * @param location the player's current location.
     */
    public void setLocation(Integer location) {
        this.location = location;
    }

    /**
     * Returns the player's current location.
     *
     * @return the player's current location.
     */
    public Integer getLocation() {
        return location;
    }

    /**
     * Returns the player's current tickets.
     *
     * @return the player's current tickets.
     */
    public Map<Ticket, Integer> getTickets() {
        return tickets;
    }

    /**
     * Sets the player's current tickets.
     *
     * @param tickets the player's current tickets.
     */
    public void setTickets(Map<Ticket, Integer> tickets) {
        this.tickets = tickets;
    }

    /**
     * Adds a ticket to the player's current tickets.
     *
     * @param ticket the ticket to be added.
     */
    public void addTicket(Ticket ticket) {
        incTicket(true, ticket);
    }

    /**
     * Removes a ticket to the player's current tickets.
     *
     * @param ticket the ticket to be removed.
     */
    public void removeTicket(Ticket ticket) {
        incTicket(false, ticket);
    }

    // Increments the ticket count for a particular Ticket.
    // @param inc the boolean to decide whether to increment (true) or decrement (false).
    // @param ticket the Ticket whose number is to be changed.
    private void incTicket(boolean inc, Ticket ticket) {
        Integer ticketCount = tickets.get(ticket);
        if (inc) ticketCount++;
        else ticketCount--;
        tickets.remove(ticket);
        tickets.put(ticket, ticketCount);
    }

    public boolean hasTickets(Move move) {
        if (move instanceof MoveTicket) return hasTickets((MoveTicket) move);
        else if (move instanceof MoveDouble) return hasTickets((MoveDouble) move);
        else return true;
    }

    private boolean hasTickets(MoveTicket move) {
        if (tickets.get(move.ticket) > 0) return true;
        else return false;
    }

    private boolean hasTickets(MoveDouble move) {
        if (tickets.get(Ticket.Double) > 0) {
            MoveTicket move1 = (MoveTicket) move.move1;
            MoveTicket move2 = (MoveTicket) move.move2;
            if (move1.ticket.equals(move2.ticket)) {
                return tickets.get(move1.ticket) > 1;
            } else {
                return hasTickets(move1) && hasTickets(move2);
            }
        }
        return false;
    }

    public List<Move> getValidMoves(ScotlandYardGraph graph)
    {
      List<Move> moves = getSingleMoves(graph, getLocation());
      List<Move> doubleMoves = new ArrayList<Move>();
      if (getColour() != Colour.Black && moves.size() == 0)
      {
        moves.add(MovePass.instance(getColour()));
      }
      else if (getNumberOfTickets(Ticket.Double) > 0)
      {
        doubleMoves = getDoubleMoves(graph, moves);
      }
      moves.addAll(doubleMoves);
      return moves;
    }

    private List<Move> getDoubleMoves(ScotlandYardGraph graph, List<Move> singleMoves)
    {
      MoveTicket firstMoveTicket, secondMoveTicket;
      MoveDouble doubleMove;
      List<Move> doubleMoves = new ArrayList<Move>();
      for (Move firstMove: singleMoves)
      {
        firstMoveTicket = (MoveTicket) firstMove;
        for (Move nextMove: getSingleMoves(graph, firstMoveTicket.target))
        {
          secondMoveTicket = (MoveTicket) nextMove;
          doubleMove = MoveDouble.instance(getColour(), firstMoveTicket, secondMoveTicket);
          if (firstMoveTicket.ticket == secondMoveTicket.ticket && getNumberOfTickets(firstMoveTicket.ticket) > 1) { doubleMoves.add(doubleMove); }
          else if (firstMoveTicket.ticket != secondMoveTicket.ticket) { doubleMoves.add(doubleMove); }
        }
      }
      return doubleMoves;
    }

    private List<Move> getSingleMoves(ScotlandYardGraph graph, Integer location)
    {
      MoveTicket moveTicket, secretMove;
      List<Move> moves = graph.generateMoves(colour, location);
      List<Move> allMoves = new ArrayList<Move>();
      for (Move move: moves)
      {
        if (move instanceof MoveTicket)
        {
          moveTicket = (MoveTicket) move;
          if (hasTickets(moveTicket))
          {
            allMoves.add(move);
          }
          if (getTickets().get(Ticket.Secret) > 0 )
          {
            secretMove = MoveTicket.instance(getColour(), Ticket.Secret, moveTicket.target);
            allMoves.add(secretMove);
          }
        }
      }
      return allMoves;
    }

    public Integer getNumberOfTickets(Ticket ticket)
    {
      return tickets.get(ticket);
    }

    public void play(Move move){

      if (move instanceof MoveTicket) { play((MoveTicket) move); }
      else if (move instanceof MoveDouble)
      {
        removeTicket(Ticket.Double);
      }

    }

    private void play(MoveTicket move)
    {
      setLocation(move.target);
      removeTicket(move.ticket);
    }
}
