package scotlandyard;

import java.util.List;

public class PoliceData implements User {
  private Player player;
  private Integer location;
  private Colour colour;

  public PoliceData(Player player, Integer location, Colour colour)
  {
    this.player = player;
    this.location = location;
    this.colour = colour;
  }

  public Colour getColour() { return colour; }

  public Player getPlayer() { return player; }

  public Integer getLocation() { return location; }

  public void setLocation(Integer location) { this.location = location; }

  public boolean hasTickets(Move move) { return true; }

  public List<Move> getValidMoves(ScotlandYardGraph graph) { return graph.generateMoves(colour, getLocation()); }

  public Integer getNumberOfTickets(Ticket ticket) { return 0; }

  public void play(Move move) { setLocation(((MoveTicket) move).target); }

}
