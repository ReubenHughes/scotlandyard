package scotlandyard;

import graph.*;

import java.util.*;

public class ScotlandYardGraph extends UndirectedGraph<Integer, Transport> {

    public List<Move> generateMoves(Colour player, Integer location)
    {
      List<Move> moves = new ArrayList<Move>();
      MoveTicket moveTicket;
      List<Edge<Integer, Transport>> allEdges = this.getEdgesFrom(this.getNode(location));
      if (allEdges != null)
      {
        for (Edge<Integer, Transport> move: allEdges)
        {
          moveTicket = MoveTicket.instance(player, Ticket.fromTransport(move.getData()),move.getTarget().getIndex());
          moves.add(moveTicket);
        }
      }
      return moves;
    }
}
